﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using webApp.PaymentGateway.Models;

namespace webApp.PaymentGateway
{
    public class Functions
    {
        public string url { get; set; }
        public string app_user { get; set; }
        public string app_password { get; set; }

        public Functions()
        {
            url = "https://do62009-pgw-be-paymentgateway-dda-dev.apps.ocpdev.pttdigital.com/pgwdda/rest/service/doservice";
            app_user = "ngv";
            app_password = "ZnbGi6kcaG";
        }

        public string CallWebService(string requestJson, out bool isError)
        {
            string result = "";
            isError = false;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            //request.ContentType = "application/json";
            //request.ContentLength = DATA.Length;
            using (Stream webStream = request.GetRequestStream())
            using (StreamWriter requestWriter = new StreamWriter(webStream, System.Text.Encoding.UTF8))
            {
                requestWriter.Write(requestJson);
            }

            try
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    string response = responseReader.ReadToEnd();
                    result = HttpUtility.UrlDecode(response);
                }
            }
            catch (Exception ex)
            {
                isError = true;
                result = ex.Message;
            }

            return result;
        }

        public string GetRequestTransactionId()
        {
            return DateTime.Now.ToString("yyyyMMddHHmmssfff");
        }

        /// <summary>
        /// Register Station Mapping Bank Account Direct Approve API
        /// </summary>
        /// <returns></returns>
        public string F20000016()
        {
            // sold_to|ship_to|bank_account | 000000000000000contract_hold
            string soldTo = "0010000001";
            string shipTo = "00000001";
            string bankAccount = "0123456789";
            string contractHold = "000000001000000";
            string reqMsg = string.Format("{0}|{1}|{2}|{3}"
                , soldTo, shipTo, bankAccount, contractHold);
            string encryptedMsg = RsaEncryption.Encrypt(reqMsg);

            Request.Rootobject request = new Request.Rootobject();
            request.function_id = "20000016";
            request.app_user = "ngv";
            request.app_password = "ZnbGi6kcaG";
            request.req_transaction_id = GetRequestTransactionId();
            request.state_name = "";
            request.req_parameters = new List<Request.Req_Parameters>();
            request.req_parameters.Add(new Request.Req_Parameters
            {
                k = "channel",
                v = "WEB"
            });

            request.req_parameters.Add(new Request.Req_Parameters
            {
                k = "encrypt_msgreq",
                v = encryptedMsg
            });
            request.req_parameters.Add(new Request.Req_Parameters
            {
                k = "sof",
                v = "DDA"
            });
            request.req_parameters.Add(new Request.Req_Parameters
            {
                k = "bank_code",
                v = "KBANK"
            });
            request.req_parameters.Add(new Request.Req_Parameters
            {
                k = "system",
                v = "NGV"
            });
            request.req_parameters.Add(new Request.Req_Parameters
            {
                k = "company",
                v = "10"
            });
            request.extra_xml = "";
            string requestJson = new JavaScriptSerializer().Serialize(request);

            string result = CallWebService(requestJson, out bool isError);
            return result;
        }

        /// <summary>
        /// Register Station Mapping Bank Account Direct Debit Online API
        /// </summary>
        /// <returns></returns>
        public string F20000017()
        {
            // sold_to|ship_to|bank_account
            string soldTo = "0010000001";
            string shipTo = "00000001";
            string bankAccount = "0123456789";
            string reqMsg = string.Format("{0}|{1}|{2}"
                , soldTo, shipTo, bankAccount);
            string encryptedMsg = RsaEncryption.Encrypt(reqMsg);

            Request.Rootobject request = new Request.Rootobject();
            request.function_id = "20000017";
            request.app_user = app_user;
            request.app_password = app_password;
            request.req_transaction_id = GetRequestTransactionId();
            request.state_name = "";
            request.req_parameters = new List<Request.Req_Parameters>();
            request.req_parameters.Add(new Request.Req_Parameters
            {
                k = "channel",
                v = "WEB"
            });

            request.req_parameters.Add(new Request.Req_Parameters
            {
                k = "encrypt_msgreq",
                v = encryptedMsg
            });
            request.req_parameters.Add(new Request.Req_Parameters
            {
                k = "sof",
                v = "DD"
            });
            request.req_parameters.Add(new Request.Req_Parameters
            {
                k = "bank_code",
                v = "KBANK"
            });
            request.req_parameters.Add(new Request.Req_Parameters
            {
                k = "system",
                v = "NGV"
            });
            request.req_parameters.Add(new Request.Req_Parameters
            {
                k = "company",
                v = "10"
            });
            request.extra_xml = "";
            string requestJson = new JavaScriptSerializer().Serialize(request);

            string result = CallWebService(requestJson, out bool isError);
            return result;
        }

        /// <summary>
        /// List Account Station API
        /// </summary>
        /// <returns></returns>
        public string F20000022()
        {
            string soldToShipTo = "0010000001|00000001";
            string encryptedMsg = RsaEncryption.Encrypt(soldToShipTo);

            Request.Rootobject request = new Request.Rootobject();
            request.function_id = "20000022";
            request.app_user = app_user;
            request.app_password = app_password;
            request.req_transaction_id = "20200310161342761";
            request.state_name = "";
            request.req_parameters = new List<Request.Req_Parameters>();
            request.req_parameters.Add(new Request.Req_Parameters
            {
                k = "channel",
                v = "WEB"
            });
            request.req_parameters.Add(new Request.Req_Parameters
            {
                k = "system",
                v = "NGV"
            });
            request.req_parameters.Add(new Request.Req_Parameters
            {
                k = "encrypt_msgreq",
                v = encryptedMsg //RSA(sold_to|ship_to)
            });
            request.req_parameters.Add(new Request.Req_Parameters
            {
                k = "company",
                v = "10"
            });
            request.extra_xml = "";
            string requestJson = new JavaScriptSerializer().Serialize(request);

            string result = CallWebService(requestJson, out bool isError);
            return result;
        }

        /// <summary>
        /// Query Payment On Hold Configuration API
        /// </summary>
        /// <returns></returns>
        public string F20000025()
        {
            Request.Rootobject request = new Request.Rootobject();
            request.function_id = "20000025";
            request.app_user = "ngv";
            request.app_password = "ZnbGi6kcaG";
            request.req_transaction_id = "20200310161342761";
            request.state_name = "";
            request.req_parameters = new List<Request.Req_Parameters>();
            request.req_parameters.Add(new Request.Req_Parameters
            {
                k = "channel",
                v = "WEB"
            });
            request.req_parameters.Add(new Request.Req_Parameters
            {
                k = "system",
                v = "NGV"
            });
            request.extra_xml = "";
            string requestJson = new JavaScriptSerializer().Serialize(request);
            string result = CallWebService(requestJson, out bool isError);
            return result;
        }
    }
}