﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webApp.PaymentGateway.Models
{
    public class Request
    {
        public class Rootobject
        {
            public string function_id { get; set; }
            public string app_user { get; set; }
            public string app_password { get; set; }
            public string req_transaction_id { get; set; }
            public string state_name { get; set; }
            public List<Req_Parameters> req_parameters { get; set; }
            public string extra_xml { get; set; }
        }

        public class Req_Parameters
        {
            public string k { get; set; }
            public string v { get; set; }
        }
    }
}