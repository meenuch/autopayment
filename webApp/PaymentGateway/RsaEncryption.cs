﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace webApp.PaymentGateway
{
    public static class RsaEncryption
    {
        public static string GetPublicKey()
        {
            string projectPath = AppDomain.CurrentDomain.BaseDirectory;
            string publicKeyPath = @"PaymentGateway\NGV\ngv_key-dev\ngv_public.xml";
            return File.ReadAllText(Path.Combine(projectPath, publicKeyPath));
        }

        public static string GetPrivateKey()
        {
            string projectPath = AppDomain.CurrentDomain.BaseDirectory;
            string privateKeyPath = @"PaymentGateway\NGV\ngv_key-dev\ngv_private.xml";
            return File.ReadAllText(Path.Combine(projectPath, privateKeyPath));
        }

        public static string Encrypt(string plainText, bool doOAEPPadding = true)
        {
            ////get a stream from the string
            //StringReader sr = new StringReader(GetPublicKey());
            ////we need a deserializer
            //var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
            ////get the object back from the stream
            //RSAParameters pubKey = (RSAParameters)xs.Deserialize(sr);

            //RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            //rsa.ImportParameters(pubKey);
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(2048);
            //rsa.ex
            rsa.FromXmlString(GetPublicKey());
            byte[] plainTextBytes = Encoding.ASCII.GetBytes(plainText);
            byte[] encryptedData = rsa.Encrypt(plainTextBytes, doOAEPPadding);
            string base64Encrypted = Convert.ToBase64String(encryptedData);
            //string encryptedString = Encoding.UTF8.GetString(encryptedData);
            return base64Encrypted;
        }

        public static string Decrypt(string encryptedText, bool doOAEPPadding = true)
        {
            //UnicodeEncoding ByteConverter = new UnicodeEncoding();
            //RSAParameters parameters = new RSAParameters
            //{
            //    Modulus = ByteConverter.GetBytes("AJ65D/mX/7KWR2gB/n9YZEehI4K10WOpHGhlOC9/0v210ZnbGiEcFV1HitdXhlt9b8PjTnzq+bLvECp3xMXyoSs="),
            //    Exponent = ByteConverter.GetBytes("AQAB"),
            //    P = ByteConverter.GetBytes("APZeLW3WccwoMhiZA/LwlbvxPGCuAFVRBeQRZtJkLHeL"),
            //    Q = ByteConverter.GetBytes("AKTtrCl0uHXwTGmzdRHhMdWfnKTe9xlFrhPLgMQfyrDh"),
            //    DP = ByteConverter.GetBytes("Ol+GFAhDXkQH2pwmu2EVmTpHPAD+Qzi3VJpq6l6hypE="),
            //    DQ = ByteConverter.GetBytes("AKPLHpW1Q5RtZ5X6EaQGH/XmJC1g+cnxd6ZVDra4VBZB"),
            //    InverseQ = ByteConverter.GetBytes("NitDz8hPAeWtanp3Ud0PnDg7hrYnso/42cdXzfCkhpk="),
            //    D = ByteConverter.GetBytes("AIgf0zGOSH9THx52R1vT5P0Bl8DsSg4c5P4rgQpLqBppygEQjrMJ2NUvTKI8vf/q+AvyECeqSW4GyIsM0n9IOoE="),
            //};

            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            //rsa.ImportParameters(parameters);
            string privateKeyXml = GetPrivateKey();
            rsa.FromXmlString(privateKeyXml);  // server decrypting data with private key
            byte[] encryptedTextBytes = Convert.FromBase64String(encryptedText);
            var decryptedBytes = rsa.Decrypt(encryptedTextBytes, doOAEPPadding);
            var decryptedData = Encoding.ASCII.GetString(decryptedBytes);
            return decryptedData.ToString();
        }

        public static void aaa()
        {
            //Chilkat.PublicKey publicKey = new Chilkat.PublicKey();
            //bool b = publicKey.LoadXml(GetPublicKey());
            //Chilkat.Rsa rsaPublic = new Chilkat.Rsa();
            //b = rsaPublic.ImportPublicKey(GetPublicKey());
            //string encrptedMsg = rsaPublic.EncryptStringENC("1234", false);

            //Chilkat.PrivateKey privateKey = new Chilkat.PrivateKey();
            //b = privateKey.LoadXml(GetPrivateKey());
            //string s = obj.GetXml();

            //RSACryptoServiceProvider

            //string projectPath = AppDomain.CurrentDomain.BaseDirectory;
            ////string privateKeyPath = @"PaymentGateway\NGV\ngv_key-dev\ngv_private.txt";
            ////string privateKeyBase64 = File.ReadAllText(Path.Combine(projectPath, privateKeyPath));
            ////byte[] privateKeyBytes = Convert.FromBase64String(privateKeyBase64);

            //string privateKeyPath = @"PaymentGateway\NGV\ngv_key-dev\ngv_private.pem";
            //byte[] privateKeyBytes = File.ReadAllBytes(Path.Combine(projectPath, privateKeyPath));

            ////byte[] privateKeyPKCS8 =  RSAKeyUtils.PrivateKeyToPKCS8(privateKeyBytes);
            ////RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            ////RSAKeyUtils.PrivateKeyToPKCS8
            //RSACryptoServiceProvider rsa = RSAKeyUtils.DecodeRSAPrivateKey(privateKeyBytes);
            //string xml = rsa.ToXmlString(true);

            //byte[] importedPrivateKeyBytes = Convert.FromBase64String("MIIEowIBAAKCAQEAtuHuMvnE3fYz/eom8mF7xxLXPETjfUpVYHL6AUVwXKKCrZ6uFszh2bY7ADoiIpuPTS0NVNYeKLlu6T59qzyr0bnT8KgXdZJFVzYWIh8VOLTb2zghlu8BeBoJP9t/a34Mj7gGJUZWBgECopEGCXrFwyIvXSW0JU5YrJinyyPhUjUXLeyowJUXoLwEafOHBRRiZZv1SNlntQOAenHFB/1uv2Y5xmdHm7xl+jjeo469u+pY535RCekmam9hWbZcn/WZEf9BZEmE2i4v7Qj72V92LrSUSSjF6w6lgU3B+Byu0XR8ZeUjD+FP9ZKvJyEtxlxOVaADQ+/udghUr1PnfXIHRQIDAQABAoIBAHG1UBFJ0unfJrx9VfHmQruoL0M94eQIz8TEOEWKEy7FrFKfEscCZHqlH1Io0wiJiDQICv3wk5fmk9taC3DorDweOnSrTsq/Q3XSHzjf8qXrbbeD0v6xZEx0g8O8iiEfolfJp6iNbvcUsbq6SPKj70pAewqDYtq/N8s4rztS98nQQ5PAQ10Zv3X910lc0vnPlPKew4y7w6hwb/pbiEBLbbV9tYAVaAm63n/EYxjo3UCRA0BJZxhJbRdEQzCEzClXlo6txx7nGQz/omv574P22ASCAfyrFNhaT20DR+ilIXe+edBpJehD6Q/lpO56STawb9xCAk6+aerM5dcIBMTETRECgYEA3RdnfpY1Z8KtOT1rVUeI094vylP1HONrcXNSsV0sbNPSwoK91UpO0mfsLLWxLtjvX52yixhfKxQ0QLYq3nyC8SRj2GE07aO1HZ81MI/9Y6ynJkw+J0nsSc8cBpu1hEUwjTgQu0GnKZtbjaZKZ7dMbtzycpUUrVjf/+34Ds2RC4sCgYEA08Ib+c6EMouffAtcEFN0fsI0HTavxJkGjZC2KuN2LMLeddiDoAKNNrl1kEwHL2ImP7g9ED4U97BJ43ekiwq4sbCDhnw+Ydxtdw2K2D3osbYIqhUPTM86YkjOkBZiU/TutVyacOTOwrzvNBLZfVn24Kxb2ehGLbsh8ar7OPsC0m8CgYBvEETDSH3Hg/o02O/ERU0s8V6cixSE0JG2yjHuO1oHyVkEsVzfepaiB+aShytc10lYhQWd7j5Qi7O8FkbuwSmeLaOinSJThnuDR+kWXh7yJVdKW96VKgNzCIGRqELFEWTUbCeric2Jjyusuq6B74iL4J5ChTV/5zePf2GvPgDxBQKBgEbgwdxrc3Q3p7otUzNju6px9l+Q3mQ/lCHuPgs892EkGLH5NpVoRTv7943E5OLHR2YslMPLA9mj+BdbBIhFnmxbWKq7C2ZWEY23yrF2h1x0QZIsWmKrOi+LjxhLZb4UTZIG3OrMqygjdS1I4eNDKY8qIdReGp9T85igUlZQLjGhAoGBAI3/3blQyHMUd1t9FbfQNDlkTAYWqUX+9Wgt34AGXjVg68Z96SzAw7rO0flnskLr6TlA/Ns21cfV1XYosK3oExbkaxqTyLHdAufKH0LPSZyaUQdQWKa9U3qMDJy78WJbaA7RmGuVfdB3mE60/iksXs0kC6R9E27gUPjopHp7Ccz2");
            //var privKeyPKCS8 = RSAKeyUtils.PrivateKeyToPKCS8(importedPrivateKeyBytes);
            //string pkcs8privKeyBase64 = Convert.ToBase64String(privKeyPKCS8);

            //Console.WriteLine("------   PRIVATE KEY TO EXPORT   ------");
            //Console.WriteLine(pkcs8privKeyBase64);
            //Console.ReadKey();

            //try
            //{
            //    //Create a UnicodeEncoder to convert between byte array and string.
            //    UnicodeEncoding ByteConverter = new UnicodeEncoding();

            //    //Create byte arrays to hold original, encrypted, and decrypted data.
            //    byte[] dataToEncrypt = ByteConverter.GetBytes("Data to Encrypt");
            //    byte[] encryptedData;
            //    byte[] decryptedData;

            //    //Create a new instance of RSACryptoServiceProvider to generate
            //    //public and private key data.
            //    using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            //    {
            //        //Pass the data to ENCRYPT, the public key information
            //        //(using RSACryptoServiceProvider.ExportParameters(false),
            //        //and a boolean flag specifying no OAEP padding.
            //        encryptedData = RSAEncrypt(dataToEncrypt, RSA.ExportParameters(false), false);

            //        //Pass the data to DECRYPT, the private key information
            //        //(using RSACryptoServiceProvider.ExportParameters(true),
            //        //and a boolean flag specifying no OAEP padding.
            //        decryptedData = RSADecrypt(encryptedData, RSA.ExportParameters(true), false);

            //        //Display the decrypted plaintext to the console.
            //        Console.WriteLine("Decrypted plaintext: {0}", ByteConverter.GetString(decryptedData));
            //    }
            //}
            //catch (ArgumentNullException)
            //{
            //    //Catch this exception in case the encryption did
            //    //not succeed.
            //    Console.WriteLine("Encryption failed.");
            //}
        }

        public static byte[] RSAEncrypt(byte[] DataToEncrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        {
            try
            {
                byte[] encryptedData;
                //Create a new instance of RSACryptoServiceProvider.
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    //Import the RSA Key information. This only needs
                    //toinclude the public key information.
                    RSA.ImportParameters(RSAKeyInfo);

                    //Encrypt the passed byte array and specify OAEP padding.
                    //OAEP padding is only available on Microsoft Windows XP or
                    //later.
                    encryptedData = RSA.Encrypt(DataToEncrypt, DoOAEPPadding);
                }
                return encryptedData;
            }
            //Catch and display a CryptographicException
            //to the console.
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public static byte[] RSADecrypt(byte[] DataToDecrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        {
            try
            {
                byte[] decryptedData;
                //Create a new instance of RSACryptoServiceProvider.
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    //Import the RSA Key information. This needs
                    //to include the private key information.
                    RSA.ImportParameters(RSAKeyInfo);

                    //Decrypt the passed byte array and specify OAEP padding.
                    //OAEP padding is only available on Microsoft Windows XP or
                    //later.
                    decryptedData = RSA.Decrypt(DataToDecrypt, DoOAEPPadding);
                }
                return decryptedData;
            }
            //Catch and display a CryptographicException
            //to the console.
            catch (CryptographicException e)
            {
                Console.WriteLine(e.ToString());

                return null;
            }
        }
    }
}