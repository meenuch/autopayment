﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Paging.ascx.cs" Inherits="webApp.UserControl.Paging" %>

<div class="col-12 col-sm-auto form-inline">
    <asp:Label ID="Label47" runat="server" Text="Label">จำนวนแถว:&nbsp</asp:Label>
    <asp:DropDownList ID="ddlPageSize" runat="server" CssClass="form-control form-control-sm" AutoPostBack="true">
        <asp:ListItem Value="5">5</asp:ListItem>
        <asp:ListItem Value="10" Selected="True">10</asp:ListItem>
        <asp:ListItem Value="15">15</asp:ListItem>
        <asp:ListItem Value="20">20</asp:ListItem>
    </asp:DropDownList>
</div>
<div class="col-12 col-sm-auto text-right">
    <div class="btn-group">
        <asp:LinkButton ID="btnFirst" runat="server" CssClass="btn btn-sm btn-primary"><i class="fa fa-fast-backward"></i></asp:LinkButton>&nbsp
        <asp:LinkButton ID="btnPrevious" runat="server" CssClass="btn btn-sm btn-primary"><i class="fa fa-step-backward"></i></asp:LinkButton>&nbsp
        <asp:Label ID="Label49" runat="server" Text="Label" Style="padding-top: 5px">หน้า:&nbsp</asp:Label>
        <asp:DropDownList ID="ddlPageIndex" CssClass="form-control form-control-sm" DataValueField="Idx" DataTextField="Idx_Display" runat="server" AutoPostBack="true"></asp:DropDownList>&nbsp
        <asp:LinkButton ID="btnNext" runat="server" CssClass="btn btn-sm btn-primary"><i class="fa fa-step-forward"></i></asp:LinkButton>&nbsp
        <asp:LinkButton ID="btnLast" runat="server" CssClass="btn btn-sm btn-primary"><i class="fa fa-fast-forward"></i></asp:LinkButton>&nbsp
    </div>
</div>
<div class="col-12 col-sm-auto text-right" style="padding-top: 5px">
    <asp:Label ID="lblRowCount" CssClass="text-success" runat="server"></asp:Label>
</div>
