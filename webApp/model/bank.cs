﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webApp.model
{
    public class bank
    {
        public string bankname { get; set; }
        public string banknameCode { get; set; }
        public string bankAccount { get; set; }
        public string banktype { get; set; }
        public string bankCode { get; set; }
    }
}