﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Site1.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="webApp.Pages.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .time-frame {
            color: black;
            text-align: center;
            border: 1px solid black;
            padding: 20px;
            border-radius: 5px;
            width: 250px;
            margin: 0 auto;
            /*margin: 0px 30% 0px 30%;*/
            /*min-width: 250px;*/
            /*background-color: #000000;
            color: #ffffff;
            width: 300px;
            font-family: Arial;*/
        }

            .time-frame > div {
                width: 100%;
                text-align: center;
            }

        #date-part {
            font-size: 1.2em;
        }

        #time-part {
            font-size: 2em;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="container-fluid cf mt-3">

                <div class="row text-center">
                    <div class="col">
                        <br />
                        <asp:Label ID="Label1" runat="server" Text="Welcome" Style="font-size: 4em"></asp:Label><br />
                        <asp:Label ID="lblUserLogin" runat="server" Text="" Style="font-size: 3em"></asp:Label>
                    </div>

                    <div style="width: 100%; margin-top: 50px">
                        <div class='time-frame'>
                            <span id='time-part'></span>
                            <br />
                            <span id='date-part'></span>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
</script>
</asp:Content>