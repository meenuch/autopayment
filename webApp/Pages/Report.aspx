﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Site1.Master" AutoEventWireup="true" CodeBehind="Report.aspx.cs" Inherits="webApp.Pages.Report" %>

<%@ Register Src="~/UserControl/Paging.ascx" TagPrefix="uc1" TagName="Paging" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid cf mt-5 mr-5">
        <!-----------------------Header---------------------->
        <ul class="breadcrumbMap">
            <li><a href="Home.aspx"><i class="fa fa-home"></i></a></li>
            <li><a href="#"><i class="fas fa-file-alt"></i>&nbsp รายงาน</a></li>
            <li style="display: none;"><a href="#"></a></li>
        </ul>
        <!-----------------------Button---------------------->
        <div class="row" style="padding-bottom: 5px !important">
            <div class="col-auto mr-auto">
                <div class="btn-group">
                    <button class="btn btn-outline-info" type="button" data-toggle="collapse" data-target="#Filter" aria-expanded="false" aria-controls="Filter">
                        <i class="fa fa-filter"></i>&nbsp ค้นหา...
                    </button>
                </div>
            </div>
            <div class="col-auto">
                <asp:LinkButton ID="btnExport" runat="server" CssClass="btn btn-success"><i class="fas fa-file-excel"></i>&nbsp Export</asp:LinkButton>
            </div>
        </div>
        <!-----------------------Filter---------------------->
        <div class="collapse" id="Filter">
            <div class="card card-body" style="margin-bottom: 5px">
                <div class="row">
                    <div class="col-4">
                        <label for="basic-url">Sold To :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id=""><i class="fas fa-search"></i></span>
                            </div>
                            <asp:TextBox ID="txtShipto" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-4">
                        <label for="basic-url">Ship To :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id=""><i class="fas fa-search"></i></span>
                            </div>
                            <asp:TextBox ID="txtSoldto" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-4">
                        <label for="basic-url">Shipping Point :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id=""><i class="fas fa-search"></i></span>
                            </div>
                            <asp:TextBox ID="txtShippingPoint" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-8">
                        <label for="basic-url">สถานี :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id=""><i class="fas fa-search"></i></span>
                            </div>
                            <asp:TextBox ID="txtStation" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-4">
                        <label for="basic-url">BillingNo :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id=""><i class="fas fa-search"></i></span>
                            </div>
                            <asp:TextBox ID="txtBillingNo" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-6">
                        <label for="basic-url">เริ่มวันที่ :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id=""><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <asp:TextBox ID="txtCreateDateStart" CssClass="form-control" runat="server" type="date"></asp:TextBox>&nbsp
                        </div>
                    </div>
                    <div class="col-6">
                        <label for="basic-url">ถึงวันที่ :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id=""><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <asp:TextBox ID="txtCreateDateEnd" runat="server" CssClass="form-control" type="date"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <br />
                <div class="form-group col-12 text-center mb-0">
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-primary"><i class="fas fa-search"></i>&nbsp ค้นหา</asp:LinkButton>
                </div>
            </div>
        </div>
        <!-----------------------Pager---------------------->
        <div class="row justify-content-end Pager">
            <div class="col-12 col-sm-auto form-inline">
                <asp:Label ID="Label48" runat="server" Text="Label">จัดเรียงโดย:&nbsp</asp:Label>
                <div class="custom-control-inline">
                    <asp:DropDownList ID="ddlSortExpression" runat="server" CssClass="form-control form-control-sm" AutoPostBack="true">
                        <asp:ListItem Value="CreateDate" Selected="True">CreateDate</asp:ListItem>
                        <%--                        <asp:ListItem Value="PlanName">PlanName</asp:ListItem>
                        <asp:ListItem Value="PackageName">PackageName</asp:ListItem>
                        <asp:ListItem Value="PaymentStatusCode">PaymentStatusCode</asp:ListItem>--%>
                    </asp:DropDownList>&nbsp
                            <asp:Label ID="Label50" runat="server" Text="Label" CssClass="col-form-label">:&nbsp</asp:Label>&nbsp
                            <asp:DropDownList ID="ddlSortDirection" runat="server" CssClass="form-control form-control-sm" AutoPostBack="true">
                                <asp:ListItem Value="ASC">ASC</asp:ListItem>
                                <asp:ListItem Value="DESC" Selected="True">DESC</asp:ListItem>
                            </asp:DropDownList>
                </div>
            </div>
            <uc1:Paging runat="server" ID="Paging" />
        </div>
        <!-----------------------Gridview---------------------->
        <div class="row justify-content-center">
            <div class="col-12 table-responsive">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" AllowPaging="true">
                    <%--<PagerStyle CssClass="pagination-ys" />--%>
                    <PagerSettings Visible="false" />
                    <Columns>
                        <asp:BoundField DataField="SoldTo" HeaderText="SoldTo" SortExpression="SoldTo" />
                        <asp:BoundField DataField="ShipTo" HeaderText="ShipTo" SortExpression="ShipTo" />
                        <asp:BoundField DataField="ShippingPoint" HeaderText="ShippingPoint" SortExpression="ShippingPoint" />
                        <asp:BoundField DataField="Site_Name" HeaderText="ชื่อสถานี" SortExpression="Site_Name" />
                        <asp:BoundField HeaderText="ตัดเงินใน Hold" />
                        <asp:BoundField HeaderText="จำนวนเงินที่ Hold" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
