﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace webApp.Pages
{
    public partial class Home : System.Web.UI.Page
    {
        public p_UserAdLogin_SelectResult usrLogin
        {
            get { return Session["usrLogin"] as p_UserAdLogin_SelectResult; }
            set { Session["usrLogin"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblUserLogin.Text = usrLogin.firstName + "  " + usrLogin.lastName;
        }
    }
}