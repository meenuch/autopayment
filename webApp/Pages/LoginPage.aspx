﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginPage.aspx.cs" Inherits="webApp.Pages.LoginPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kanit|Prompt">
    <link href="../Content/all.css" rel="stylesheet" />
    <link href="../fontawesome/css/all.css" rel="stylesheet" />
    <style>
        .formLogin {
            border-radius: 20px;
            box-shadow: 0 5px 8px 0 rgba(0, 0, 0, 0.2), 0 9px 26px 0 rgba(0, 0, 0, 0.19);
            background: rgba(255, 255, 255, 1);
            background-color: #FAFAFA;
            padding-bottom: 20px;
            padding-top: 30px;
            margin-top: 125px;
            margin-bottom: 20px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <script src="../Scripts/jquery-3.0.0.slim.min.js"></script>
        <script src="../Scripts/umd/popper.min.js"></script>
        <script src="../Scripts/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script>
            function alertMSG(text) {
                Swal.fire({
                    icon: 'error',
                    title: 'Login Failed',
                    text: text
                })
            }
        </script>
        <div class="container formLogin col-12 col-sm-8 col-md-6 col-lg-4 col-xl-3">
            <h2 class="text-center">
                <asp:Image ID="Image1" runat="server" ImageUrl="../images/ptt5.jpg" CssClass="img-fluid" Width="400" Height="150" /></h2>
            <h2 class="text-center mb-3">NGV Auto Payment</h2>
            <asp:Panel runat="server" DefaultButton="btnLogin">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id=""><i class="fas fa-user"></i></span>
                    </div>
                    <asp:TextBox ID="txtUsername" runat="server" placeholder="Username" required="required" CssClass="form-control" autofocus="autofocus" />
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id=""><i class="fas fa-lock"></i></span>
                    </div>
                    <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" required="required" CssClass="form-control" TextMode="Password" />
                </div>
                <div class="text-center">
                    <asp:Button ID="btnLogin" runat="server" Text="เข้าสู่ระบบ" CssClass="btn btn-lg btn-primary" OnClick="btnLogin_Click" />
                </div>
            </asp:Panel>
        </div>
    </form>
</body>
</html>