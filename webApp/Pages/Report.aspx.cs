﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace webApp.Pages
{
    public partial class Report : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView1.CssClass = "table";
            NGVDBDataContext db = new NGVDBDataContext();
            List<Site> listsite = db.Sites.Take(3).ToList();
            GridView1.DataSource = listsite;
            GridView1.DataBind();
        }

    }
    }