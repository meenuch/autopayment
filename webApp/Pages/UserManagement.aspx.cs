﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;

namespace webApp.Pages.UserSetup
{
    public partial class UserManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            NAPDBDataContext db = new NAPDBDataContext();
            List<UserLogin> listUser = db.UserLogins.ToList();
            gvUserManagement.DataSource = listUser;
            gvUserManagement.DataBind();
            gvUserManagement.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
}