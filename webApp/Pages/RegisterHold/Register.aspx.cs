﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using webApp.model;

namespace webApp.Pages.RegisterHold
{
    public partial class Register : System.Web.UI.Page
    {
        public List<bank> bankList
        {
            get { return Session["bankList"] as List<bank>; }
            set { Session["bankList"] = value; }
        }

        public List<v_SiteSelect> siteList
        {
            get { return Session["siteList"] as List<v_SiteSelect>; }
            set { Session["siteList"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string type = Request.QueryString["Type"];
                if (type.Equals("Update"))
                {
                    lbType.Text = "แก้ไข";
                    LoadDataUpdate();
                }
                else if (type.Equals("Regis"))
                {
                    bankList = new List<bank>();
                    lbType.Text = "ลงทะเบียน";
                    lbSaveDraft.Visible = true;
                    lbRegister.Visible = true;
                    lbCancle.Visible = true;
                    txtState.ReadOnly = true;
                    txtShippingPoint.ReadOnly = true;
                    txtShipTo.ReadOnly = true;
                    txtSoldTo.ReadOnly = true;
                    txtProvince.ReadOnly = true;
                    txtSector.ReadOnly = true;
                    txtStation.ReadOnly = true;
                    txtAddress.ReadOnly = true;
                }
                LoadData();
            }
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            //GridView2.CssClass = "table table-bordered";
            GridView2.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        protected void LoadData()
        {
            NAPDBDataContext db = new NAPDBDataContext();
            siteList = db.v_SiteSelects.ToList();
            GridView1.DataSource = siteList;
            GridView1.DataBind();
            ddlBankName.DataTextField = "BankName";
            ddlBankName.DataValueField = "BankCode";
            ddlBankName.DataSource = db.MasterBankCodes;
            ddlBankName.DataBind();
            ddlBankType.DataTextField = "SofName";
            ddlBankType.DataValueField = "SofCode";
            ddlBankType.DataSource = db.MasterSofs;
            ddlBankType.DataBind();
            LoadBankGV();
        }

        protected void LoadDataUpdate()
        {
            LinkButton1.Visible = false;
            NGVDBDataContext db = new NGVDBDataContext();
            Site siteList = db.Sites.Single(x => x.ShippingPoint == "4101");
            txtState.Text = siteList.SITE_NAME;
            txtShippingPoint.Text = siteList.ShippingPoint;
            txtShipTo.Text = siteList.ShipTo;
            txtSoldTo.Text = siteList.ShipTo;
            txtAddress.Text = "27 1 ถ. รามอินทรา แขวง รามอินทรา เขตคันนายาว กรุงเทพมหานคร 10230";
            txtProvince.Text = "กรุงเทพมหานคร";
            txtSector.Text = "กลาง";
            txtStation.Text = "NGV";
            txtState.ReadOnly = true;
            txtShippingPoint.ReadOnly = true;
            txtShipTo.ReadOnly = true;
            txtSoldTo.ReadOnly = true;
            txtProvince.ReadOnly = true;
            txtSector.ReadOnly = true;
            txtStation.ReadOnly = true;
            txtAddress.ReadOnly = true;
            txtAmt.ReadOnly = true;
            txtAmt.Text = "1,000,000";
            bankList = new List<bank>();
            bank bankModal = new bank()
            {
                bankname = "กสิกรไทย",
                bankAccount = "066-5-07850-4",
                banktype = "Direct Debit"
            };
            bank bankModal2 = new bank()
            {
                bankname = "กสิกรไทย",
                bankAccount = "066-5-07850-4",
                banktype = "Direct Approve"
            };
            bankList.Add(bankModal);
            bankList.Add(bankModal2);
            GridView2.DataSource = bankList;
            GridView2.DataBind();
            txtEmailouthold.Text = "znarisorn.s@ptt.com,namem@gmail.com";
            txtSmsouthold.Text = "0890200202,0919823456";
            txtAreaEmailouthold.Text = "znarisorn.s@ptt.com,namem@gmail.com";
            txtAreaSMSouthold.Text = "0890200202,0919823456";
            lbSubmit.Visible = true;
            lbUnRegis.Visible = true;
            lbCancle.Visible = true;
        }

        protected void LinkButton5_Click(object sender, EventArgs e)
        {
            bank bankModal = new bank()
            {
                bankname = ddlBankName.SelectedItem.Text,
                banknameCode = ddlBankName.SelectedValue,
                bankAccount = tbBankAccount.Text,
                banktype = ddlBankType.SelectedItem.Text,
                bankCode = ddlBankType.SelectedValue
            };
            bankList.Add(bankModal);
            LoadBankGV();
        }

        protected void LoadBankGV()
        {
            GridView2.DataSource = bankList;
            GridView2.DataBind();
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "cmdSelect")
            {
                var siteSelect = siteList.Single(x => x.ShippingPoint == e.CommandArgument.ToString());
                txtState.Text = siteSelect.SITE_NAME;
                txtAddress.Text = siteSelect.SITE_ADDRESS1;
                txtShippingPoint.Text = siteSelect.ShippingPoint;
                txtShipTo.Text = siteSelect.ShipTo;
                txtSoldTo.Text = siteSelect.SoldTo;
                txtProvince.Text = siteSelect.ProvinceName;
                txtSector.Text = siteSelect.RegionName;
                txtStation.Text = siteSelect.SiteType_Name;
            }
        }
    }
}