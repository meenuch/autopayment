﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace webApp.Pages.RegisterHold
{
    public partial class Details : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            NGVDBDataContext db = new NGVDBDataContext();
            List<Site> listsite = db.Sites.ToList();
            GridView1.DataSource = listsite;
            GridView1.DataBind();
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Pages/RegisterHold/Register.aspx?Type=Update");
        }
    }
}