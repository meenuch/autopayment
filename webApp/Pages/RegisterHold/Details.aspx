﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Site1.Master" AutoEventWireup="true" CodeBehind="Details.aspx.cs" Inherits="webApp.Pages.RegisterHold.Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container cf2 mt-3 mb-0 " <%--style="max-width: 95%;"--%>>
        <!-----------------------Header---------------------->
        <ul class="breadcrumbMap">
            <li><a href="Home.aspx"><span style="font-size: 22px;"><i class="fa fa-home"></i></span></a></li>
            <li><a href="#"><span style="font-size: 22px;"><i class="fas fa-file-alt"></i>&nbsp ลงทะเบียนสถานี</span></a></li>
            <li><a href="#" style="display: none"></a></li>
        </ul>
    </div>
    <div class="container-fluid cf mt-2">
        <!-----------------------Button---------------------->
        <div class="row" style="padding-bottom: 10px !important">
            <div class="col-auto mr-auto">
                <div class="btn-group">
                    <button class="btn btn-outline-info" type="button" data-toggle="collapse" data-target="#Filter" aria-expanded="false" aria-controls="Filter">
                        <i class="fa fa-filter"></i>&nbsp ค้นหา...
                    </button>
                </div>
            </div>
            <div class="col-auto">
                <asp:LinkButton ID="btnRegister" runat="server" CssClass="btn btn-info" PostBackUrl="~/Pages/RegisterHold/Register.aspx?Type=Regis"><i class="fas fa-file-alt"></i>&nbsp ลงทะเบียน</asp:LinkButton>
                <asp:LinkButton ID="btnExport" runat="server" CssClass="btn btn-success"><i class="fas fa-file-excel"></i>&nbsp Export</asp:LinkButton>
            </div>
        </div>
        <!-----------------------Filter---------------------->
        <div class="collapse" id="Filter">
            <div class="card card-body" style="margin-bottom: 5px">
                <div class="row">
                    <div class="col-4">
                        <label for="basic-url">Ship To :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id=""><i class="fas fa-search"></i></span>
                            </div>
                            <asp:TextBox ID="txtSoldto" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-4">
                        <label for="basic-url">Sold To :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id=""><i class="fas fa-search"></i></span>
                            </div>
                            <asp:TextBox ID="txtShipto" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-4">
                        <label for="basic-url">Shipping Point :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id=""><i class="fas fa-search"></i></span>
                            </div>
                            <asp:TextBox ID="txtShippingPoint" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-8">
                        <label for="basic-url">สถานี :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id=""><i class="fas fa-search"></i></span>
                            </div>
                            <asp:TextBox ID="txtStation" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-4">
                        <label for="basic-url">สถานะลงทะเบียน :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id=""><i class="fas fa-list"></i></span>
                            </div>
                            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                    </div>
                </div>
                <br />
                <div class="form-group col-12 text-center mb-0">
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-primary"><i class="fas fa-search"></i>&nbsp ค้นหา</asp:LinkButton>
                </div>
            </div>
        </div>
        <!-----------------------Gridview---------------------->
        <div class="row justify-content-center">
            <div class="col-12 table-responsive">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" SkinID="dataTableSkin">
                    <HeaderStyle Wrap="false" Width="100%" />
                    <Columns>
                        <asp:BoundField DataField="ShippingPoint" HeaderText="Shipping Point" SortExpression="ShippingPoint" />
                        <asp:BoundField DataField="Site_Name" HeaderText="ชื่อสถานี" SortExpression="Site_Name" />
                        <asp:BoundField DataField="SoldTo" HeaderText="Sold To" SortExpression="SoldTo" />
                        <asp:BoundField DataField="ShipTo" HeaderText="Ship To" SortExpression="ShipTo" />
                        <asp:TemplateField HeaderText="จำนวนเงินในสัญญา (บาท)">
                            <ItemTemplate>
                                <asp:Label ID="lbAmt" runat="server" Text="1,000,000"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="สถานะการลงทะเบียน">
                            <ItemTemplate>
                                <asp:Label ID="lbStatus" runat="server" Text="ลงทะเบียน"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-primary" OnClick="LinkButton2_Click">แก้ไข</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>