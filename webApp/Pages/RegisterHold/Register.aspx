﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Site1.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="webApp.Pages.RegisterHold.Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .modal-dialog {
            max-width: 90% !important;
        }

        .bgLink {
            color: blue;
        }

        /*table tr th {
            background-color: #E9ECEF;
        }*/
        .cfheader {
            border-top-left-radius: 20px;
            border-top-right-radius: 20px;
            padding: 0.75rem 1.25rem;
            margin-bottom: 0;
            background-color: #2980b9;
            border-bottom: 1px solid rgba(0, 0, 0, 0.125);
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="container cf2 mt-3 mb-0 " <%--style="max-width: 95%;"--%>>
                <ul class="breadcrumbMap">
                    <li><a href="Home.aspx"><span style="font-size: 22px;"><i class="fa fa-home"></i></span></a></li>
                    <li>
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Pages/RegisterHold/Details.aspx"><span style="font-size: 22px;"><i class="fas fa-file-alt"></i>&nbsp ลงทะเบียนสถานี</span></asp:HyperLink></li>
                    <li><a href="#"><span style="font-size: 22px;">
                        <asp:Label ID="lbType" runat="server"></asp:Label></span></a></li>
                </ul>
            </div>
            <div class="cf1 mt-2">
                <div class="cfheader">
                    <span style="font-size: 20px;" class="header text-white ml-1">ข้อมูลสถานี</span>
                </div>
                <div class="bodyDetail">
                    <div class="row">
                        <div class="col-6">
                            <label for="basic-url">สถานี :</label>
                            <div class="input-group">
                                <asp:TextBox ID="txtState" runat="server" CssClass="form-control"></asp:TextBox>
                                <div class="input-group-append">
                                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-outline-primary" data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fas fa-th-list"></i></asp:LinkButton>
                                    <%--                        <button class="btn btn-outline-secondary" type="button">Button</button>--%>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <label for="basic-url">ที่อยู่ :</label>
                            <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-4">
                            <label for="basic-url">Ship To :</label>
                            <asp:TextBox ID="txtShipTo" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-4">
                            <label for="basic-url">Sold To :</label>
                            <asp:TextBox ID="txtSoldTo" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-4">
                            <label for="basic-url">Shipping Point :</label>
                            <asp:TextBox ID="txtShippingPoint" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-4">
                            <label for="basic-url">ประเภทสถานี :</label>
                            <asp:TextBox ID="txtStation" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-4">
                            <label for="basic-url">จังหวัด :</label>
                            <asp:TextBox ID="txtProvince" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-4">
                            <label for="basic-url">ภาค :</label>
                            <asp:TextBox ID="txtSector" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cf1 mt-2">
                <div class="cfheader"><span style="font-size: 20px;" class="header text-white ml-1">วงเงิน</span></div>
                <div class="bodyDetail">
                    <div class="col-12">
                        <div class="form-inline">
                            <div class="form-group mb-2">
                                <label for="staticEmail2">เงินในสัญญาที่กันไว้</label>
                            </div>
                            <div class="form-group mx-sm-3 mb-2">
                                <asp:TextBox ID="txtAmt" runat="server" CssClass="form-control" placeholder="0,000,000"></asp:TextBox>
                            </div>
                            <div class="form-group mb-2">
                                <asp:Label ID="Label1" runat="server" Text="บาท"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cf1 mt-2">
                <div class="cfheader">
                    <span style="font-size: 20px;" class="header text-white ml-1">บัญชีธนาคาร</span>
                </div>
                <div class="bodyDetail">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="True" CssClass="table table-bordered table-striped">
                                <%--<EmptyDataTemplate>
                                <div align="center" class="text-danger">ไม่พบข้อมูล</div>
                            </EmptyDataTemplate>--%>
                                <HeaderStyle Wrap="false" Width="100%" CssClass="bg-primary text-white" />
                                <Columns>
                                    <asp:BoundField DataField="bankname" HeaderText="ธนาคาร" SortExpression="bankname" HeaderStyle-Width="23%" ItemStyle-Width="23%" />
                                    <asp:BoundField DataField="bankAccount" HeaderText="หมายเลขบัญชี" SortExpression="bankAccount" HeaderStyle-Width="33%" ItemStyle-Width="33%" />
                                    <asp:BoundField DataField="banktype" HeaderText="ประเภทบัญชี" SortExpression="banktype" HeaderStyle-Width="31%" ItemStyle-Width="31%" />
                                    <asp:TemplateField HeaderText="" HeaderStyle-Width="8%" ItemStyle-Width="8%">
                                        <ItemTemplate>
                                            <span class="font-weight-bold" style="color: blue; font-size: 15px;">Default</span>
                                            <asp:LinkButton ID="LinkButton6" runat="server" CssClass="ml-1 mt-1"><i class="fas fa-trash" style="color:red;"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">
                            <asp:DropDownList ID="ddlBankName" runat="server" CssClass="buttonSearch">
                            </asp:DropDownList>
                        </div>
                        <div class="col-4">
                            <asp:TextBox ID="tbBankAccount" runat="server" CssClass="buttonSearch" placeholder="หมายเลขบัญชี"></asp:TextBox>
                        </div>

                        <div class="col-4">
                            <asp:DropDownList ID="ddlBankType" runat="server" CssClass="buttonSearch">
                            </asp:DropDownList>
                        </div>
                        <div class="col-1">
                            <asp:LinkButton ID="LinkButton5" runat="server" CssClass="btn btn-primary" OnClick="LinkButton5_Click">เพิ่ม</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cf1 mt-2">
                <div class="cfheader">
                    <span style="font-size: 20px;" class="header text-white ml-1">ช่องทางการติดต่อ</span>
                </div>
                <div class="bodyDetail">
                    <asp:HiddenField ID="hdnTab" runat="server" Value="1" />
                    <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active bgLink" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">นอกวงเงิน Hold</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link bgLink" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">ในวงเงิน Hold</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link bgLink" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">ในวงเงิน Hold เกินกำหนดวัน</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                                <div class="col-8">
                                    <label for="basic-url">Email บุคคลที่ต้องการแจ้งข้อความ :</label>
                                    <asp:TextBox ID="txtEmailouthold" runat="server" CssClass="form-control" placeholder="email@email.com,email2@email.com"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-8">
                                    <label for="basic-url">SMS บุคคลที่ต้องการแจ้งข้อความ :</label>
                                    <asp:TextBox ID="txtSmsouthold" runat="server" CssClass="form-control" placeholder="0xxxxxxxx,0xxxxxxxx"></asp:TextBox>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-8">
                                    <label for="basic-url">Area Manager Email :</label>
                                    <asp:TextBox ID="txtAreaEmailouthold" runat="server" CssClass="form-control" placeholder="email@email.com,email2@email.com"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-8">
                                    <label for="basic-url">Area Manager SMS :</label>
                                    <asp:TextBox ID="txtAreaSMSouthold" runat="server" CssClass="form-control" placeholder="0xxxxxxxx,0xxxxxxxx"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="row">
                                <div class="col-8">
                                    <label for="basic-url">Email บุคคลที่ต้องการแจ้งข้อความ :</label>
                                    <asp:TextBox ID="TextBox11" runat="server" CssClass="form-control" placeholder="email@email.com,email2@email.com"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-8">
                                    <label for="basic-url">SMS บุคคลที่ต้องการแจ้งข้อความ :</label>
                                    <asp:TextBox ID="TextBox12" runat="server" CssClass="form-control" placeholder="0xxxxxxxx,0xxxxxxxx"></asp:TextBox>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-8">
                                    <label for="basic-url">Area Manager Email :</label>
                                    <asp:TextBox ID="TextBox13" runat="server" CssClass="form-control" placeholder="email@email.com,email2@email.com"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-8">
                                    <label for="basic-url">Area Manager SMS :</label>
                                    <asp:TextBox ID="TextBox14" runat="server" CssClass="form-control" placeholder="0xxxxxxxx,0xxxxxxxx"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                            <div class="row">
                                <div class="col-8">
                                    <label for="basic-url">Email บุคคลที่ต้องการแจ้งข้อความ :</label>
                                    <asp:TextBox ID="TextBox15" runat="server" CssClass="form-control" placeholder="email@email.com,email2@email.com"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-8">
                                    <label for="basic-url">SMS บุคคลที่ต้องการแจ้งข้อความ :</label>
                                    <asp:TextBox ID="TextBox16" runat="server" CssClass="form-control" placeholder="0xxxxxxxx,0xxxxxxxx"></asp:TextBox>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-8">
                                    <label for="basic-url">Area Manager Email :</label>
                                    <asp:TextBox ID="TextBox17" runat="server" CssClass="form-control" placeholder="email@email.com,email2@email.com"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-8">
                                    <label for="basic-url">Area Manager SMS :</label>
                                    <asp:TextBox ID="TextBox18" runat="server" CssClass="form-control" placeholder="0xxxxxxxx,0xxxxxxxx"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid text-center mb-3">
                <asp:LinkButton ID="lbSaveDraft" runat="server" CssClass="btn btn-info" Visible="false">Save Draft</asp:LinkButton>
                <asp:LinkButton ID="lbRegister" runat="server" CssClass="btn btn-info" Visible="false">ลงทะเบียน</asp:LinkButton>
                <asp:LinkButton ID="lbSubmit" runat="server" CssClass="btn btn-info" Visible="false">ยืนยัน</asp:LinkButton>
                <asp:LinkButton ID="lbUnRegis" runat="server" CssClass="btn btn-danger" Visible="false">ยกเลิกลงทะเบียน</asp:LinkButton>
                <asp:LinkButton ID="lbCancle" runat="server" CssClass="btn btn-secondary" Visible="false" PostBackUrl="~/Pages/RegisterHold/Details.aspx">ยกเลิก</asp:LinkButton>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <%--modal--%>
    <div class="modal fade bd-example-modal-lg modal-ku" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="modal-title" style="font-size: 20px;">กรุณาเลือกสถานี</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row justify-content-center mr-2 ml-2 mb-3 mt-2">
                        <div class="col-12 table-responsive">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" SkinID="dataTableSkin" OnRowCommand="GridView1_RowCommand">
                                <HeaderStyle Wrap="false" Width="100%" />
                                <Columns>
                                    <asp:BoundField DataField="ShippingPoint" HeaderText="ShippingPoint" SortExpression="ShippingPoint" />
                                    <asp:BoundField DataField="Site_Name" HeaderText="ชื่อสถานี" SortExpression="Site_Name" />
                                    <asp:BoundField DataField="SoldTo" HeaderText="SoldTo" SortExpression="SoldTo" />
                                    <asp:BoundField DataField="ShipTo" HeaderText="ShipTo" SortExpression="ShipTo" />
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-primary" CommandName="cmdSelect" CommandArgument='<%# Eval("ShippingPoint")%>'>เลือก</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>