﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Site1.Master" AutoEventWireup="true" CodeBehind="UserManagement.aspx.cs" Inherits="webApp.Pages.UserSetup.UserManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container cf mt-5 mr-5">
        <ul class="breadcrumbMap">
            <li><a href="Home.aspx"><i class="fa fa-home"></i></a></li>
            <li><a href="#"><i class="fas fa-user-alt"></i>&nbsp UserManagement</a></li>
            <li><a href="#">แก้ไข</a></li>
        </ul>
        <div class="col-12">
            <asp:GridView ID="gvUserManagement" runat="server" AutoGenerateColumns="False" SkinID="dataTableSkin">
                <HeaderStyle Wrap="false" Width="100%" />
                <Columns>
                    <asp:BoundField DataField="username" HeaderText="username" SortExpression="username" />
                    <asp:BoundField DataField="firstName" HeaderText="firstName" SortExpression="firstName" />
                    <asp:BoundField DataField="lastName" HeaderText="lastName" SortExpression="lastName" />
                    <asp:BoundField DataField="email" HeaderText="email" SortExpression="email" />
                    <asp:BoundField DataField="adgroup" HeaderText="adgroup" SortExpression="adgroup" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>