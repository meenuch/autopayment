﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace webApp.Pages
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        public string GeneratedMenu
        {
            get { return Session["GeneratedMenu"] as string; }
            set { Session["GeneratedMenu"] = value; }
        }

        public string GeneratedMenuSM
        {
            get { return Session["GeneratedMenuSM"] as string; }
            set { Session["GeneratedMenuSM"] = value; }
        }

        public p_UserAdLogin_SelectResult usrLogin
        {
            get { return Session["usrLogin"] as p_UserAdLogin_SelectResult; }
            set { Session["usrLogin"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lbUsername.Text = usrLogin.username;
            Literal1.Text = GeneratedMenu;
            Literal2.Text = GeneratedMenuSM;
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("~/Pages/LoginPage.aspx");
        }
    }
}