﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.DirectoryServices.Protocols;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace webApp.Pages
{
    public partial class LoginPage : System.Web.UI.Page
    {
        public string GeneratedMenu
        {
            get { return Session["GeneratedMenu"] as string; }
            set { Session["GeneratedMenu"] = value; }
        }

        public p_UserAdLogin_SelectResult usrLogin
        {
            get { return Session["usrLogin"] as p_UserAdLogin_SelectResult; }
            set { Session["usrLogin"] = value; }
        }

        public string GeneratedMenuSM
        {
            get { return Session["GeneratedMenuSM"] as string; }
            set { Session["GeneratedMenuSM"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtUsername.Text))
                txtUsername.Focus();
            else if (string.IsNullOrEmpty(txtPassword.Text))
                txtPassword.Focus();
            else
            {
                try
                {
                    GeneratedMenu = "";
                    GeneratedMenuSM = "";
                    NAPDBDataContext db = new NAPDBDataContext();
                    usrLogin = db.p_UserAdLogin_Select(txtUsername.Text.Trim()).SingleOrDefault();
                    if (usrLogin != null && usrLogin.adlogin == true)
                    {
                        string group = usrLogin.adgroup;
                        string username = usrLogin.username;
                        string password = txtPassword.Text;
                        bool result = ValidateAD(group, username, password);
                        //bool result = true;
                        if (result != true)
                        {
                            //alert(this.Page, "Invalid username or password");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "alertMSG('Invalid username or password')", true);
                        }
                        else
                        {
                            GenerateMenu(usrLogin.userGroupId);
                            Response.Redirect("~/Pages/Home.aspx");
                        }
                    }
                    else
                    {
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "alertMSG('Invalid username or password')", true);
                        string password = DAL.Encryption.EncryptStringAES(txtPassword.Text.Trim());
                        p_UserLogin_SelectResult ursLogin = db.p_UserLogin_Select(txtUsername.Text.Trim(), password).SingleOrDefault();
                        if (ursLogin != null)
                        {
                            GenerateMenu(ursLogin.userGroupId);
                            Response.Redirect("~/Pages/Home.aspx");
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "alertMSG('Invalid username or password')", true);
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        private bool ValidateAD(string Group, string Username, string Password)
        {
            // LDAP://202.44.0.80
            LdapConnection connection = new LdapConnection("202.44.0.80");
            NetworkCredential credential = new NetworkCredential(Username, Password, Group);
            connection.Credential = credential;
            connection.Bind();
            return true;
        }

        protected void GenerateMenu(int groupid)
        {
            NAPDBDataContext db = new NAPDBDataContext();
            List<p_UserMenu_SelectResult> usermenu = db.p_UserMenu_Select(groupid).ToList();

            List<p_UserMenuParent_SelectResult> menuParent = db.p_UserMenuParent_Select(groupid).ToList();
            List<p_UserMenuChild_SelectResult> menuChild = db.p_UserMenuChild_Select(groupid).ToList();
            foreach (var item in menuParent)
            {
                var childList = menuChild.Where(x => x.menuParent == item.menuCode).ToList();
                if (childList.Count > 0)
                { //have chil
                    GeneratedMenu += string.Format(@"<li id='li{2}' onclick=""myAccFunc('" + item.menuCode + @"')"" class='bgColor' style='border-bottom: 1px solid grey;'><i class='{0}'></i><span style='font-size: 15px;'>&nbsp {1}<i id='IC{2}'class='fas fa-angle-up float-right mt-4'></i></span></li>", item.menuIcon, item.menuName, item.menuCode);
                    GeneratedMenu += string.Format("<ul id='A{0}' class='w3-show w3-ul ml-0'>", item.menuCode);
                    GeneratedMenuSM += string.Format(@"<li id='li2{1}' onclick=""myAccFunc('" + item.menuCode + @"')"" class='bgColor' style='border-bottom: 1px solid grey;'><i class='{0}'></i><span style='font-size: 12px'><i id='ID{1}' class='fas fa-angle-up mb-2'></i></span></li>", item.menuIcon, item.menuCode);
                    GeneratedMenuSM += string.Format("<ul id='B{0}' class='w3-show w3-ul ml-0'>", item.menuCode);
                    foreach (var value in childList)
                    {
                        GeneratedMenu += string.Format(" <a href='{0}' style='text-decoration: none;'><li class='select' style='border-bottom: 1px solid grey;'><i class='{1} ml-3'></i><span style='font-size: 15px;'>&nbsp {2}</span></li></a>", RootWebURL + value.menuUrl, value.menuIcon, value.menuName);
                        GeneratedMenuSM += string.Format(" <a href='{0}' style='text-decoration: none;'><li class='select' style='border-bottom: 1px solid grey;'><i class='{1} ml-2'></i></li></a>", RootWebURL + value.menuUrl, value.menuIcon);
                    }
                    GeneratedMenu += "</ul>";
                    GeneratedMenuSM += "</ul>";
                }
                else
                {
                    GeneratedMenu += string.Format("<a href='{0}' style='text-decoration: none;'><li class='select' style='border-bottom: 1px solid grey;'><i class='{1}'></i><span style='font-size: 15px;'>&nbsp {2}</span></li></a>", RootWebURL + item.menuUrl, item.menuIcon, item.menuName);
                    GeneratedMenuSM += string.Format("<a href='{0}'><li class='select' style='border-bottom: 1px solid grey;'><i class='{1}' style='text-decoration: none;'></i></li></a>", RootWebURL + item.menuUrl, item.menuIcon);
                }
            }
        }

        private string RootWebURL
        {
            get
            {
                string appPath = string.Empty;
                HttpContext context = HttpContext.Current;
                if (context != null)
                {
                    appPath = string.Format("{0}://{1}{2}{3}",
                                            context.Request.Url.Scheme,
                                            context.Request.Url.Host,
                                            context.Request.Url.Port == 80
                                                ? string.Empty
                                                : ":" + context.Request.Url.Port,
                                            context.Request.ApplicationPath);
                }

                if (!appPath.EndsWith("/")) appPath += "/";

                return appPath;
            }
        }
    }
}